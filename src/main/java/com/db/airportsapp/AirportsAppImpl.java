package com.db.airportsapp;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class AirportsAppImpl implements IAirportsApp{
    private static List<Airport> airports = null;

    public AirportsAppImpl() {
        try {
            airports = Files.readAllLines(Paths.get("C:/Users/Yashvi/Downloads/airports.csv")).stream().skip(1).map(AirportsAppImpl::stringToAirport).collect(Collectors.toList());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public List<Airport> findAirportByCode(String code) {
        if (code == null || code.length() == 0)
            throw new IllegalArgumentException();
        //TODO Need to implement this when you have the acutal data
        return airports.stream().filter((each) -> each.getCode().equals(code)).collect(Collectors.toList());
    }


    @Override
    public List<Airport> findAirportByName(String name) {
        if (name == null || name.length() == 0)
            throw new IllegalArgumentException();
        //TODO Need to implement this when you have the acutal data
        return airports.stream().filter((each) -> each.getName().equals(name)).collect(Collectors.toList());
    }

    @Override
    public List<Airport> findAirportByLatitude(String latitude) {
        if (latitude == null || latitude.length() == 0)
            throw new IllegalArgumentException();
        //TODO Need to implement this when you have the acutal data
        return airports.stream().filter((each) -> each.getLatitude().equals(latitude)).collect(Collectors.toList());
    }

    @Override
    public List<Airport> findAirportByLongitude(String longitude) {
        if (longitude == null || longitude.length() == 0)
            throw new IllegalArgumentException();
        //TODO Need to implement this when you have the acutal data
        return airports.stream().filter((each) -> each.getLongitude().equals(longitude)).collect(Collectors.toList());
    }

    @Override
    public List<Airport> findAirportByAddress(String address) {
        if (address == null || address.length() == 0)
            throw new IllegalArgumentException();
        //TODO Need to implement this when you have the acutal data
        return airports.stream().filter((each) -> each.getAddress().equals(address)).collect(Collectors.toList());
    }

    public static Airport stringToAirport(String row) {
        String trimmedData = row.replaceAll("\"", "");
        String[] cols = trimmedData.split(",");
        Airport airport = new Airport();
        airport.setCode(cols[0]);
        airport.setName(cols[3]);
        airport.setLatitude(cols[4]);
        airport.setLongitude(cols[5]);
        airport.setAddress(cols[10]);
        return airport;
    }
}
