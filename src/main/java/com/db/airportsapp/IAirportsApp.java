package com.db.airportsapp;

import java.util.List;

public interface IAirportsApp {
    List<Airport> findAirportByCode(String code);

    List<Airport> findAirportByName(String name);

    List<Airport> findAirportByLatitude(String latitude);

    List<Airport> findAirportByLongitude(String longitude);

    List<Airport> findAirportByAddress(String address);
}
