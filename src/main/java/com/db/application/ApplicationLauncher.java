package com.db.application;

public class ApplicationLauncher {
    public static void main(String[] args) {
//        // is a relation.... that is getting qualified
//        // the is  a relation ship will always return me true.
//        System.out.println(new ApplicationImpl() instanceof IApplication);
//        IApplication application = new ApplicationImpl();
//        System.out.println(application.deposit("0001",1001));
//        System.out.println(application.withdraw("0001", 993));


        /**
         * Disengage with the implementation .....
         * Disengage with the Classes for business implementation only work with interfaces
         * Decoupling will help in enchancement of the future proof coding
         */
        IApplication application1 = ApplicationFactory.getApplication();
        application1.getVersion();
        application1.deposit("xx", 1);
    }
}

