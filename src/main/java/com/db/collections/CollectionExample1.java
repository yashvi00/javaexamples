package com.db.collections;

import java.util.*;

public class CollectionExample1 {
    public static void main(String[] args){
//testSet();
        testLists();
    }
    public static void testLists() {
        List list = new ArrayList();
        for (int i = 0; i <= 10; i++) {
            list.add("xxxxx");
            System.out.println(list.size());
        }
        list.clear();
        List<String> name = new ArrayList<String>();
        name.add("Nilesh");
        System.out.println(name.get(0));

    }

    public static void testSet() {
        Set set = new HashSet();
        set.add("Nilesh");
        set.add("Nilesh1");
        set.add("Nilesh2");
        set.add("Nilesh3");
        set.add("Nilesh4");
        System.out.println(set.size());

    }

    public static void testMap(){
        Map<String, String> users = new HashMap<String,String>();
        users.put("user1", "xxxxx");
        users.put("user2", "users2");
        System.out.println(users.get("user1"));
    }


}
