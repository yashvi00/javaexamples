package com.db.ioexamples;

import java.io.*;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.Buffer;
import java.nio.file.Files;
import java.nio.file.Paths;


public class FileReadWriteExample {
    public static void main(String[] args){
     readFilesNativeWithStreams();
//        long startTime = System.currentTimeMillis();
//        // readFile();
////        readFileUsingReader();
//        readFilesNativeWithStreams();
//        long endTime = System.currentTimeMillis();
//        System.out.println(endTime-startTime);

    }

    public static void readFile() {
        InputStream is = null;
        try {
            is = new FileInputStream("C:/Users/Yashvi/Desktop");
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            System.out.println(new String(buffer));
        } catch (Exception e) {
            System.out.println(e);
        } finally {
            if( is != null)
            {
                try{
                    is.close();
                }catch(IOException e){
                    //TODO
                }
            }
        }
    }

    public static void readFileUsingReader() {
        // a varaible to store each line
        String data = "";
        // the buffered reader
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("C:/Users/Yashvi/Downloads/airports.csv"));
            while ((data = reader.readLine()) != null) {
                System.out.println(data);
            }
        } catch (Exception fe) {
            System.out.println(fe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void readFilesNativeWithStreams() {
        try {
            System.out.println(Files.readString(Paths.get("C:/Users/Yashvi/Downloads/airports.csv")));
        } catch (Exception exception) {
            // exception curation
        } finally {
            // Close your resource if na open
        }

    }





    /**
     *   java.io.InputStream is; //used to read file(Binary)
     *         java.io.OutputStream os; //used to write file
     *
     *
     *         java.io.Reader reader; //read the ascii value
     *         java.io.Writer writer; //write the text content
     *
     *         java.io.File file; //File System isDirectory, isFile isReadOnly, isWrite, Absolute paths
     *
     *         java.nio.file.Files nfiles; //native io non java
     */

}
