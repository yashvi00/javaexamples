package com.db.tests;

import com.db.airportsapp.Airport;
import com.db.airportsapp.AirportAppFactory;
import com.db.airportsapp.IAirportsApp;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

public class AirportAppTest {
    @Test
    public void testFindAirportByCode() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByCode("6523");
        assertFalse(actualAirports.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByCodeBadArgument() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByCode(null);
    }

    @Test
    public void testFindAirportByName() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByName("Lowell Field");
        assertFalse(actualAirports.isEmpty());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByNameBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByName(null);
    }

    @Test
    public void testFindAirportByLatitude() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLatitude("59.94919968");
        assertFalse(actualAirports.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByLatitudeBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLatitude(null);
    }

    @Test
    public void testFindAirportByLongitude() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLongitude("-151.695999146");
        assertFalse(actualAirports.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByLongitudeBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLongitude(null);
    }


    @Test
    public void testFindAirportByAddress() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByAddress("Harvest");
        assertFalse(actualAirports.isEmpty());
    }


    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByAddressBadArgument() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByAddress(null);
    }

    @Test
    public void testFindAirportRandomly() {
        fail("Not Yet Implemented ");
    }

    @Test
    public void testFindAirportNearMe() {
        fail("Not Yet Implemented ");
    }


    @Test
    public void testCountTotalAirports() {
        fail("Not Yet Implemented ");
    }

    @Test
    public void testLogin() {
        fail("Not Yet Implemented ");
    }

    @Test
    public void testSignUp() {
        fail("Not Yet Implemented ");
    }

    @Test
    public void testHelp() {
        fail("Not Yet Implemented ");
    }

    @Test
    public void testSiteInfo() {
        fail("Not Yet Implemented ");
    }
}
